def remove_duplicates(s):
    ret = []
    for ch in s:
        if len(ret) and ret[-1] == ch:
            ret.pop()
        else:
            ret.append(ch)
    return ret

print remove_duplicates('aaabbbccc')