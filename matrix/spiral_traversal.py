def spiral_traversal(matrix):
    size = len(matrix)
    total = len(matrix)*len(matrix)
    i = 0
    start_row = 0
    end_row = size-1

    start_col = 0
    end_col = size-1

    dir = 0

    while i<total:

        if dir == 0:
            # ltr
            j = start_col
            while j <= end_col:
                print matrix[start_row][j]
                j+=1
                i += 1

            start_row +=1
            dir = 1

        if dir == 1:
            j = start_row
            while j <= end_row:
                print matrix[j][end_col]
                j+=1
                i+=1
            end_col -=1
            dir = 2

        if dir == 2:
            j = end_col
            while j>=start_col:
                print matrix[end_row][j]
                j-=1
                i+=1
            end_row -=1
            dir = 3

        if dir == 3:
            j = end_row
            while j>=start_row:
                print matrix[j][start_col]
                j-=1
                i+=1
            start_col+=1
            dir = 0

arr = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
    [13,14,15,16]
]

spiral_traversal(arr)

