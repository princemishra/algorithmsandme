
def coin_sum(coins, target):
    # given an infinite supply of coins, make target using minimum number of coins

    # initialize with -1, meaning cannot make this sum

    # extra space for 0, 0 as 0 may appear in the calculation

    size = len(coins)
    dp = [[float("inf")]*(target+1) for i in xrange(size + 1)]

    # init
    dp[0][0] = 0

    for i in xrange(size + 1):
        dp[i][0] = 0

    for i in xrange(size+1):

        for j in xrange(target + 1):
            current_coin = coins[i-1]
            current_target = j

            # cannot use current coin
            # copy the solution that existed before
            dp[i][j] = dp[i - 1][j]

            # can I use current_coin to make current_target?
            if current_coin <= current_target:
                # yes I can
                # lets use 1 coin of current_coin
                remaining = current_target-current_coin
                # how do I make remaining?
                remaining_soln = dp[i][remaining] # remember, this is 1-based?
                new_solution = 1 + remaining_soln
                if new_solution <= dp[i - 1][j]: # default is inf. any finite value is < inf
                    dp[i][j] = new_solution

    return dp[-1][-1]

def coin_sum_recursive(coins, target):
    if target == 0:
        # target made
        return 0

    # question here is, which coin do we use?
    # we use the coin using which the result is minimum

    solution_space = [coin_sum_recursive(coins, target - c) for c in coins if target >=c ]

    # use the minimum of the solution space and add 1
    return 1+min(solution_space)


#coins = [1,3,5]
coins = [9,6,5,1]
target = 11
#print coin_sum(coins, target)
print coin_sum_recursive(coins, target)
