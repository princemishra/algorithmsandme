"""
If only someone called DP "careful brute force"!
"""

def lis(arr):
    # LIS at i is 1+ max (LIS of all j < i) or 1
    dp = [1]*len(arr)
    for i in range(len(arr)):
        for j in range(i):
            c0 = arr[j] < arr[i]
            c1 = (1 + dp[j]) > dp[i]
            if c0 and c1:
                dp[i] = 1 + dp[j]
    return max(dp)


#arr = [3,10,2,1,20]
arr = [10, 22, 9, 33, 21, 50, 41, 60]
print lis(arr)