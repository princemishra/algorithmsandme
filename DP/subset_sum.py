def subset_sum_naive(arr, target):
    # terminal
    # 1. we exhaust the arr
    # 2. target is 0 => we have found the subset in the caller
    # 3. there is only 1 element, but it is

    # terminal
    if target == 0:
        # found in previous call
        return True
    if not arr:
        # we have exhausted the numbers
        return False
    # propagation
    return subset_sum_naive(arr[1:], target - arr[0]) or subset_sum_naive(arr[1:], target)


def subset_sum_dp(arr, target):
    # https://www.youtube.com/watch?v=5td2QH-x5ck

    dp = [[0] * (target + 1) for i in xrange(len(arr)+1)]

    dp[0][0] = 1

    for i in xrange(len(arr)+1):
        # we can always make 0 with an empty set
        dp[i][0] = 1

    for i in xrange(len(arr) + 1):
        for j in xrange(target + 1):
            current_element = arr[i - 1]
            current_sum = j

            # let's copy the state that existed before we introduced this element

            dp[i][j] = dp[i-1][j]

            # can I use the current element
            if current_element <= current_sum:
                remaining = current_sum - current_element
                # if we can make remaining too, we're good
                dp[i][j] = dp[i-1][remaining]

    return dp[-1][-1]


arr = [1,2,3,4,5,6,7,8,9,10]
target = 55
#print subset_sum_naive(arr, target)
print subset_sum_dp(arr, target)