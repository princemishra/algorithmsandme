# start looking from the end, if current characters match, lcs = 1 + lcs of previous items


def lcs_naive(A, B):
    # base case
    # if any string caeses to exist,there is no further checking
    if not A or not B:
        return 0

    # propagation
    if A[-1] == B[-1]:
        return 1 + lcs_naive(A[:-1], B[:-1])

    return max(lcs_naive(A[:-1], B), lcs_naive(A, B[:-1]))

A = '1234'
B = '5162'
print lcs_naive(A, B)


