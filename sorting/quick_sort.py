# Classic divide and conquer
# partition and combine

def partition(arr, start, end):
    pivot = start

    # all elements smaller than pivot should be to its left
    # all elements greater than pivot should be to ite right

    i = start + 1
    j = end

    while i < j:
        while i<end and arr[i] <= arr[pivot]:
            # find first element greater than pivot
            i+=1
        while j>start and arr[j] > arr[pivot]:
            # find first element smaller than pivot
            j-=1
        if i < j:
            # swap if they haven't crossed
            arr[i], arr[j] = arr[j], arr[i]
    ret = pivot
    if arr[j] <= arr[pivot]:
        arr[pivot], arr[j] = arr[j], arr[pivot]
        ret = j
    return ret

def quicksort(arr, start, end):
    # terminal condition
    if start >= end:
        return

    # propagation
    partition_index = partition(arr, start, end)
    quicksort(arr, start, partition_index-1)
    quicksort(arr, partition_index+1, end)


test_arr = [8,3,4,1,8,7,4,6,6,9]
quicksort(test_arr, 0, len(test_arr)-1)
print test_arr