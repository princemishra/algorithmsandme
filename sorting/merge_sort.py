# recursive merge sort


def merge(left, right):
    ret = []
    i = 0
    j = 0
    while i<len(left) and j<len(right):
        if left[i] <= right[j]:
            ret.append(left[i])
            i+=1
        else:
            ret.append(right[j])
            j += 1

    ret += left[i:]
    ret += right[j:]
    return ret


def merge_sort(arr):
    # terminal condition
    if len(arr)<2:
        return arr

    # propagation
    mid = len(arr)/2
    left = merge_sort(arr[:mid])
    right = merge_sort(arr[mid:])
    return merge(left, right)

test_arr = [2,3,4,1,8,7,4,6,6,9]
print merge_sort(test_arr)
