"""
1. build max heap
2. extract max and swap with last
3. discard last : it is the maximum elem
3. max_heapify root
"""

def max_heapify(arr, i, end):
    # recursively heapify from i
    left = 2*i+1
    right = 2*i + 2
    # is left child greater than parent?
    max_idx = i
    if left<end and arr[left] > arr[max_idx]:
        max_idx = left
    if right<end and arr[right] > arr[max_idx]:
        max_idx = right

    if i != max_idx:
        # needs swapping
        arr[i], arr[max_idx] = arr[max_idx], arr[i]
        max_heapify(arr, max_idx, end)

def build_max_heap(arr):
    last_idx = len(arr)-1
    mid = (last_idx-1)/2
    # all elements > mid are leaves
    i = mid
    while i >= 0:
        max_heapify(arr, i, last_idx)
        i-=1

def heap_sort(max_heap):
    length = len(max_heap)
    for i in xrange(length):
        end = length - i-1
        max_heap[0], max_heap[end] = max_heap[end], max_heap[0]
        max_heapify(max_heap, 0, end)

test_arr = [5,3,17,10,84,19,6,22,9]
build_max_heap(test_arr)
heap_sort(test_arr)
print test_arr