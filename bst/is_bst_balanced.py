class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def height(node):
    if not node:
        return -1
    # Imagine the leaf node. Height of leaf should be 0, right?
    # 0 = 1 + (max(-1, -1))
    return 1 + max(height(node.left), height(node.right))

def is_bst_balanced(node):
    # terminal condition
    # leaves are always balanced
    if not node:
        return True
    # a tree is balanced when height(left) and height(right) differ by max 1
    left_height = height(node.left)
    right_height = height(node.right)
    diff = abs(left_height - right_height)
    is_balanced = is_bst_balanced(node.left) and is_bst_balanced(node.right) and diff <2
    return is_balanced


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.left.left.left = Node(7)
# balanced
root.left.left.left.left = Node(8)
# not balanced

print is_bst_balanced(root)