"""
The simplest idea is -
start at root and find which of a, b exist in left, then on right
if they exist on either side, root is the LCA

if both in left, go looking in the left
if both in the right, go looking in the right
But this is terribly complex code
"""

# Second

# The idea is simple
# starting at root, the question we need to ask is :
# Which of a, b exist on : 1. either side, 2. on any one and propagate that up




class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def least_common_ancestor(root, a, b, ret):
    # find the lca of a and b

    # terminal
    if root == None:
        return None

    # The current node matches something
    if root.data == a:
        ret[0] = True
        return True
    if root.data == b:
        ret[1] = True
        return root

    # propagation

    lca_from_left = least_common_ancestor(root.left, a, b, ret)
    lca_from_right = least_common_ancestor(root.right, a, b, ret)

    if lca_from_left and lca_from_right:
        # This is the LCA
        return root

    if lca_from_left:
        return lca_from_left
    if lca_from_right:
        return lca_from_right





root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.left.left.left = Node(7)

# need to handle cases when LCA absent

print least_common_ancestor(root, 5,6, [0,0]).data
