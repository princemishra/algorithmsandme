class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

def zigzag_traversal_two_stacks(root):
    # initialize two stacks
    # we're using python lists as  stack
    s1 = []
    s2 = []
    s1.append(root)

    while len(s1) or len(s2):
        # either of them has any element

        # pop elements from first stack
        # push L, R in second stack
        while len(s1):
            cur = s1.pop()
            print cur.data
            if cur.left:
                s2.append(cur.left)
            if cur.right:
                s2.append(cur.right)

        # pop elements from second stack
        # push R, L in first stack
        while len(s2):
            cur = s2.pop()
            print cur.data
            if cur.right:
                s1.append(cur.right)
            if cur.left:
                s1.append(cur.left)




root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.left.left.left = Node(7)
root.left.left.right = Node(8)
#root.left.left.left.left = Node(8)

zigzag_traversal_two_stacks(root)