# replace every node with sum of left child + sum of right child

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def sum_tree(root):
    # terminal condition

    if root == None:
        return None

    if root.left or root.right:
        left_sum = sum_tree(root.left) if root.left else 0
        right_sum = sum_tree(root.right) if root.right else 0
        cur = root.data

        root.data = left_sum + right_sum
        return cur + root.data
    else:
        return root.data


def inorder(root):
    if root == None:
        return None
    if root.left:
        inorder(root.left)
    print root.data
    if root.right:
        inorder(root.right)

root = Node(10)
root.left = Node(6)
root.right = Node(15)
root.left.left = Node(4)
root.left.right = Node(7)
root.right.left = Node(13)
root.right.right = Node(17)

print inorder(root)
sum_tree(root)
inorder(root)