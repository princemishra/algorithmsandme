class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

def prune_outside_range(root, vrange):
    """
    process children first : postorder
    function returns node, which get assigned to parent's processing
    """
    # terminal case
    if root == None:
        return None

    # propagation
    root.right = prune_outside_range(root.right, vrange)
    root.left = prune_outside_range(root.left, vrange)

    # Now that we have ensured left and right are fine
    # process current_node

    if root.data < vrange[0]:
        # current is smaller than minimum,
        # there may be some elements in its right subtree
        return root.right

    if root.data > vrange[1]:
        # there can be some nodes in the left subtree
        return root.left

    # node is in range, do nothing
    return root

def inorder(root):
    if root == None:
        return None
    if root.left:
        inorder(root.left)
    print root.data
    if root.right:
        inorder(root.right)



root = Node(6)
root.left = Node(-13)
root.right = Node(14)
root.left.right = Node(-8)
root.right.left = Node(13)
root.right.right = Node(15)
# balanced
print "Initial"
inorder(root)
new_root = prune_outside_range(root, (-10, 13))
print "Final"
inorder(new_root)